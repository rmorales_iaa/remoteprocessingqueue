/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  04/Mar/2022
 * Time:  12h:05m
 * Description: None
 */
//=============================================================================
package com.common.RemoteProcessingQueue
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.simpleFits.SimpleFits
import com.common.logger.VisualLazyLogger
import com.common.spark.MySpark
import com.common.spark.dataFrame.MyDataframe
import com.common.util.file.MyFile
import com.common.util.path.Path
import org.apache.hadoop.io.{IntWritable, Text}
import org.apache.hadoop.mapred.TextOutputFormat
import org.apache.spark.rdd.RDD
//=============================================================================
//=============================================================================
object RemoteProcessingQueue {
  //---------------------------------------------------------------------------
  type RDD_TYPE = (String,String)
  //---------------------------------------------------------------------------
  private val fitsExtension = MyConf.c.getStringSeq("Common.fitsFileExtension")
  //---------------------------------------------------------------------------
  private val FILE_SEQUENCE_RECORD_KEY_BYE_SIZE   = 512
  private val FILE_SEQUENCE_RECORD_VALUE_BYE_SIZE = 4 * 1024
  //---------------------------------------------------------------------------
}
//=============================================================================
import RemoteProcessingQueue._
case class RemoteProcessingQueue(spark: MySpark, dir: String) extends VisualLazyLogger{
  //---------------------------------------------------------------------------
  private val sc = spark.sparkContext

   val r = loadDirectoryAsRdd
   if (r.isDefined) processRDD(r.get)
  //---------------------------------------------------------------------------
  private def processRDD(rdd: RDD[RDD_TYPE]) = {
    rdd.saveAsSequenceFile("output/rdd_kk",Some(classOf[org.apache.hadoop.io.compress.DeflateCodec]))
    val rddWritable = rdd.map(x=> (new String(x._1), new Text(x._2)))
    //rddWritable.saveAsNewAPIHadoopFile("location",rdd)
  }
  //---------------------------------------------------------------------------
  private def parseFile(path: String) : Option[String] = {
    val fits = SimpleFits(path)
    if (fits == null || !fits.isValid()) {
      error(s"Image:'$path' has a invalid FITS format")
      return None
    }
    val keyValueSeq = SimpleFits.KEY_OM_SEQ.flatMap { key=>
      val value = fits.getStringValueOrEmpty(key)
      if (value.isEmpty) {
        error(s"Image:'$path' has not the FITS record with key:'$key'")
        return None
      }
      Some(s"$key $value")
    }
    Some(keyValueSeq.mkString("\n"))
  }
  //---------------------------------------------------------------------------
  private def padKey(s:String)=
    s + (" " * (s.length - FILE_SEQUENCE_RECORD_KEY_BYE_SIZE))
  //---------------------------------------------------------------------------
  private def padValue(s:String)=
    s + (" " * (s.length - FILE_SEQUENCE_RECORD_VALUE_BYE_SIZE))
  //---------------------------------------------------------------------------
  private def loadDirectoryAsRdd(): Option[RDD[RDD_TYPE]] = {
    if (!Path.directoryExist(dir)) {
      error("Directory:'$dir' does not exist")
      None
    }
    else {
      val imageRecordSeq = Path.getSortedFileList(dir, fitsExtension).flatMap { f =>
        val path = f.getAbsolutePath
        val r = parseFile(path)
        if (r.isDefined) Some(padKey(path) -> padValue(r.get))
        else return None
      }
      Some(sc.parallelize(imageRecordSeq).cache())
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file RemoteProcessingQueue.scala
//=============================================================================
