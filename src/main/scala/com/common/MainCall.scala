/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.common
//=============================================================================
import com.common.RemoteProcessingQueue.RemoteProcessingQueue
import com.common.spark.MySpark
import com.common.logger.VisualLazyLogger
import com.common.util.time.Time._
//=============================================================================
import java.time.Instant
//=============================================================================
//=============================================================================
object MainCall {
  //---------------------------------------------------------------------------
  //timing
  private val startMs = System.currentTimeMillis
  private var mySpark: MySpark = null
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import MainCall._
case class MainCall() extends App with VisualLazyLogger {
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    // set time zone to UTC
    Instant.now.atZone(zoneID_UTC)
    mySpark= MySpark("park hadoop queue")
  }
  //---------------------------------------------------------------------------
  private def finalActions(): Unit = {
    mySpark.close(printLog = true)
    warning(s"Spark hadoop queue: ${System.currentTimeMillis - startMs} ms " )
  }
  //---------------------------------------------------------------------------
  def run(): Unit = {
    initialActions()

    new RemoteProcessingQueue(mySpark, "/home/rafa/Downloads/deleteme/fumo/2008_OG19/")

    finalActions()
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Claret.scala
//=============================================================================
