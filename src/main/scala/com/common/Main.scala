/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  10/Feb/2020
 * Time:  15h:19m
 * Description: None
 */
//=============================================================================
package com.common
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.VisualLazyLogger
//=============================================================================
//=============================================================================
object Main extends VisualLazyLogger {
  //-------------------------------------------------------------------------
  //Version
  private final val MAJOR_VERSION = 0
  private final val MINOR_VERSION = 0
  private final val COMPILATION_VERSION = 1
  private final val DATE_VERSION = "04 March 2022"
  final val VERSION = s"$MAJOR_VERSION.$MINOR_VERSION.$COMPILATION_VERSION"
  final val VERSION_AND_DATE = s"$VERSION ($DATE_VERSION)"
  final val AUTHOR="Rafael Morales (rmorales [at] iaa [dot] com)"
  final val FILIATION="UDIT. IAA-CSIC. 2018"
  final val LICENSE="http://www.apache.org/licenses/LICENSE-2.0.txt"
  //---------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf("input/configuration/main.conf")
    if (MyConf.c == null || !MyConf.c.isLoaded ) fatal(s"Error. Error parsing configuration file")
    else{
      info(s"----------------------------- Spark hadoop queue $VERSION starts -----------------------------")
      MainCall().run()
      info(s"----------------------------- Spark hadoop queue $VERSION ends -------------------------------")
      System.exit(0)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Main.scala
//=============================================================================
