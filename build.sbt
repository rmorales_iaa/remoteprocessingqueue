//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name                 := "remoteProcessingQueue"
  , version            := "0.1"
  , scalaVersion       := "2.12.10"
  , description        := "NASA's Planetary data system (PDS)"
  , organization       := "IAA-CSIC"
  , homepage           := Some(url("http://www.iaa.csic.es"))
)
//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.common.Main"
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"   //generate an error when warning raises. Enable when stable version was published
)
//-----------------------------------------------------------------------------
//dependencies versions
val apacheCommonsIO          = "2.11.0"
val apacheLoggingVersion     = "2.14.1"
val apacheCsvVersion         = "1.9.0"
val scalaReflectVersion      = "2.12.10"
val typeSafeVersion          = "1.4.1"
val scalaTestVersion         = "3.3.0-SNAP3"
val sparkVersion             = "3.2.1"
//-----------------------------------------------------------------------------
//assembly
mainClass in assembly := Some(mainClassName)
logLevel in assembly := Level.Warn

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", _*) => MergeStrategy.discard
  case _ => MergeStrategy.last
}
//-----------------------------------------------------------------------------
//tests
parallelExecution in test := false

//-----------------------------------------------------------------------------
//resolvers
//sbt-plugin
resolvers += Resolver.sonatypeRepo("public")
//-----------------------------------------------------------------------------
//external source code
val externalRootDirectory="/home/rafa/proyecto/common_scala/"

unmanagedSourceDirectories in Compile += file(externalRootDirectory + "configuration")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "logger")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "fits/simpleFits")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "spark")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/file")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/path")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/pattern")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/time")
unmanagedSourceDirectories in Compile += file(externalRootDirectory + "util/util")

excludeFilter in (Compile, unmanagedSources) := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath
    p.startsWith(externalRootDirectory + "logger/visual/") ||
      p.startsWith(externalRootDirectory + "image/jFits/")   ||
      p.startsWith(externalRootDirectory + "image/sourceDetection/")
  })}
//-----------------------------------------------------------------------------
//dependencies list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://github.com/apache/logging-log4j-scala
  , "org.apache.logging.log4j" % "log4j-api" % apacheLoggingVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLoggingVersion

  // https://mvnrepository.com/artifact/commons-io/commons-io
  , "commons-io" % "commons-io" % apacheCommonsIO

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion

  //csv management: https://mvnrepository.com/artifact/org.apache.commons/commons-csv
  ,  "org.apache.commons" % "commons-csv" % apacheCsvVersion

  //Spark and hadoop
  , "org.apache.spark" %% "spark-core" % sparkVersion
  , "org.apache.spark" %% "spark-sql" % sparkVersion

//scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
